Description: Allow overwriting the default install prefix
 The prefix is hard-coded to /usr/local in the Makefile and ignores outside
 environment variables. This patch changes this to respect the environment
 variable.
Author: Sebastian Humenda <shumenda@gmx.de>
Last-Update: 2023-02-07

Index: quickjs/Makefile
===================================================================
--- quickjs.orig/Makefile
+++ quickjs/Makefile
@@ -41,7 +41,7 @@ CONFIG_DEFAULT_AR=y
 endif
 
 # installation directory
-prefix=/usr/local
+PREFIX ?= /usr
 
 # use the gprof profiler
 #CONFIG_PROFILE=y
@@ -201,11 +201,11 @@ $(QJSC): $(OBJDIR)/qjsc.host.o \
 
 endif #CROSS_PREFIX
 
-QJSC_DEFINES:=-DCONFIG_CC=\"$(QJSC_CC)\" -DCONFIG_PREFIX=\"$(prefix)\"
+QJSC_DEFINES:=-DCONFIG_CC=\"$(QJSC_CC)\" -DCONFIG_PREFIX=\"$(PREFIX)\"
 ifdef CONFIG_LTO
 QJSC_DEFINES+=-DCONFIG_LTO
 endif
-QJSC_HOST_DEFINES:=-DCONFIG_CC=\"$(HOST_CC)\" -DCONFIG_PREFIX=\"$(prefix)\"
+QJSC_HOST_DEFINES:=-DCONFIG_CC=\"$(HOST_CC)\" -DCONFIG_PREFIX=\"$(PREFIX)\"
 
 $(OBJDIR)/qjsc.o: CFLAGS+=$(QJSC_DEFINES)
 $(OBJDIR)/qjsc.host.o: CFLAGS+=$(QJSC_HOST_DEFINES)
@@ -298,17 +298,17 @@ clean:
 	rm -rf run-test262-debug run-test262-32
 
 install: all
-	mkdir -p "$(DESTDIR)$(prefix)/bin"
+	mkdir -p "$(DESTDIR)$(PREFIX)/bin"
 	$(STRIP) qjs qjsc
-	install -m755 qjs qjsc "$(DESTDIR)$(prefix)/bin"
-	ln -sf qjs "$(DESTDIR)$(prefix)/bin/qjscalc"
-	mkdir -p "$(DESTDIR)$(prefix)/lib/quickjs"
-	install -m644 libquickjs.a "$(DESTDIR)$(prefix)/lib/quickjs"
+	install -m755 qjs qjsc "$(DESTDIR)$(PREFIX)/bin"
+	ln -sf qjs "$(DESTDIR)$(PREFIX)/bin/qjscalc"
+	mkdir -p "$(DESTDIR)$(PREFIX)/lib/quickjs"
+	install -m644 libquickjs.a "$(DESTDIR)$(PREFIX)/lib/quickjs"
 ifdef CONFIG_LTO
-	install -m644 libquickjs.lto.a "$(DESTDIR)$(prefix)/lib/quickjs"
+	install -m644 libquickjs.lto.a "$(DESTDIR)$(PREFIX)/lib/quickjs"
 endif
-	mkdir -p "$(DESTDIR)$(prefix)/include/quickjs"
-	install -m644 quickjs.h quickjs-libc.h "$(DESTDIR)$(prefix)/include/quickjs"
+	mkdir -p "$(DESTDIR)$(PREFIX)/include/quickjs"
+	install -m644 quickjs.h quickjs-libc.h "$(DESTDIR)$(PREFIX)/include/quickjs"
 
 ###############################################################################
 # examples
